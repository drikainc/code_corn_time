var webpack = require('webpack');
var path = require('path');
//important plugin to not have to manage the html view
var HtmlWebpackPlugin = require('html-webpack-plugin');
//dist file to provide our static assets important when dealing with isomorphic
const BUILD_DIR = path.resolve(__dirname, '../dist');
const APP_DIR = path.resolve(__dirname, '../src');
//documented issue https://github.com/webpack/webpack-dev-server/issues/720 when adding additional extensions this will break
const BASE_EXTENSIONS = ['.js'];


var configuration = {
  entry: {
    index: `${APP_DIR}/index.jsx`,
  },
  // devtool: '', to determine an evaltool for our project
  output: {
    path: BUILD_DIR,
    filename: '[name]_bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js[x]+/,
        loader: 'babel-loader'
      },
    ]
  },
  resolve: {
    extensions: [...BASE_EXTENSIONS, ...['.jsx']]
  },
  devtool: 'eval-source-map',
  plugins: [new HtmlWebpackPlugin(
    {
      title: 'code_corn_time',
      template: 'src/index.html'
    }
  )]
}

module.exports = configuration;