# Code Corn Time (guess why)

Learning React-Redux project `inspired`  by popcorntime

the main focus of the workshop is to get a grasp of some practices
to create isomorphic react applications.

we'll only cover partially some of the configuration of those tools

## Getting started

### Prerequisites

- node > 6.9.0
- npm > 3.10.8

### Installing

fetch the source code

```
git clone fmarcos83@bitbucket.org:drikainc/code_corn_time.git
git clone fmarcos83@bitbucket.org:drikainc/code_corn_time_components.git
```

installing dependencies

```
cd code_corn_time; npm install
cd code_corn_time_components; npm install
```

linking our two repositories with npm link

```
cd code_corn_time_components; npm link 
cd code_corn_time; npm link code_corn_time_components
```

running development server

```
npm run dev
```

running production build

```
npm run dist
```

## Running the tests

WIP

## Deployment

```
npm run dist
```

## Built With

- Babel (https://babeljs.io)
- RxJS (http://reactivex.io/rxjs/)
- Redux (http://redux.js.org/)
- React (https://reactjs.org/)
- Webpack (https://webpack.js.org/)
- Express (http://expressjs.com/)
- CSSModules (https://github.com/css-modules/css-modules)
- CSSNext (http://cssnext.io/)
- ReactCSSModules (https://github.com/gajus/react-css-modules)
- Falcor (https://www.npmjs.com/package/falcor)

... and others

## Contributing

Please read [CONTRIBUTING.md] (https://www.contributor-covenant.org/version/1/4/code-of-conduct.md) for the code of conduct, and how to send pull request to us.

## Versioning

## Authors

* ** Francisco Marcos** - *Alpha and Omega of the workshop* - [DrikaInc] https://github.com/fmarcos83/

## License

* TODO: WIP

## Acknowledgements

* Popcorntime https://popcorn-time.to/ https://popcorn-time.to/source.html
* Isomorphic 500 http://isomorphic500.herokuapp.com/
* PurpleBooth https://github.com/PurpleBooth 
